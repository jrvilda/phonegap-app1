var app = {
	inicio: function(){
		this.iniciaBotones();
		
		this.iniciaFastClick();
		
		this.iniciaHammer();
	},
	
	iniciaFastClick: function(){
		FastClick.attach(document.body);
	},
	
	iniciaBotones: function(){
		var botonClaro = document.querySelector('#claro');
		var botonOscuro = document.querySelector('#oscuro');
		
		botonClaro.addEventListener('click',this.ponloClaro,false);
		botonOscuro.addEventListener('click',this.ponloOscuro,false);
	},
	
	iniciaHammer: function(){
		var zona = document.getElementById('zona-gestos');
		var hammertime = new Hammer(zona);
		
		hammertime.get('pinch').set({ enable:true });
		hammertime.get('rotate').set({ enable:true });
		
		zona.addEventListener('webkitAnimationEnd', function(e){
			zona.className='';
		});

		hammertime.on('tap', function(ev){
			zona.className='tap';
			app.muestraTexto('tap');
		});

		hammertime.on('doubletap', function(ev){
			zona.className='doubletap';
			app.muestraTexto('doubletap');
		});
		
		hammertime.on('press', function(ev){
			zona.className='press';
			app.muestraTexto('press');

		});
		
		hammertime.on('swipe', function(ev){
			var clase = undefined;
			direccion = ev.direction;
			
			if(direccion == 4) clase = 'swipe-derecha';
			if(direccion == 2) clase = 'swipe-izquierda';
			
			app.muestraTexto(clase);

			zona.className = clase;
		});
		
		hammertime.on('rotate', function(ev){
			var umbral = 10;
			if(ev.distance > umbral)  {
					if(ev.deltaX < 0)  zona.className = 'rotateI';
					if(ev.deltaX > 0)  zona.className = 'rotateD';
				};

			app.muestraTexto(zona.className);
		});
	},
	
	ponloClaro: function(){
		document.body.className = 'claro';
		app.muestraTexto('Click claro');
	},
	
	ponloOscuro: function(){
		document.body.className = 'oscuro';
		app.muestraTexto('Click oscuro');
	},

	muestraTexto: function(text){
		document.querySelector('#info').innerHTML=text+'!';
	},
};

if('addEventListener' in document){
	document.addEventListener('DOMContentLoaded', function(){
		app.inicio();
	}, false);
}
