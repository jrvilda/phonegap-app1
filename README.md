# Ejercicio de aplicaciones PhoneGap


## Para usar este código necesitas tener instalado en el equipo Windows, PhoneGap.

	- Descarga la versión en http://phonegap.com/getstarted/
	- Abre el proyecto existente en este código. 
	- Abre el navegador en la url dada por PhoneGap.

### Para probar en un Android o en un Iphone

	- Descarga de la tienda de aplicaciones PhoneGap.
	- Abre el navegador en la url ofrecida por el cliente PhoneGap de escritorio.


### Changelog
<br>
<br>TAG v1.5
<br>	Practica de Gestos 1
<br>	Se modifica el rotate en vez de con angle con deltaX
<br>
<br>TAG v1.4
<br>	Practica de Gestos 1
<br>	Se añade animacion con hammer para TAB y ROTATE tanto IZQ como DERC.
<br>	Se mantiene todo lo anterior y se vuelve añadir la zona info donde aparece el nombre del evento.	
<br>	Ejs:
<br>		- si se pulsa una vez la zona se pone amarilla.
<br>		- si se intenta girar, este gira para el lado correspondiente.
<br>
<br>TAG v1.3
<br>	Practica de Gestos 1
<br>
<br>	Se pone animacion CSS a los botones claro y oscuro
<br>
<br>TAG v1.2
<br>	Se añade animacion al hacer realizar los gestos en el cuadrado y se quita la etiqueta de info:
<br>	Ejs:
<br>		- si se hace doble click sobre esta zona, desaparece.
<br>		- si se hace click prolongado sobre esta zona, se oscurece.
<br>		- si se desliza el dedo tanto derecha como izquierda sobre la zona, hace swip a cada lado.
<br>		- si se inenta girar en sentido del reloj, rota.
<br>
<br>TAG v1.1
<br>	Se añade una zona en la que poder realizar gestos y estos sean mostrados en la parte inferior.
<br>	Ejs:
<br>		- si se hace doble click sobre esta zona, aparecerá : DOUBLETAP!
<br>		- si se hace click sobre esta zona, aparecerá : TAP!
<br>		- si se desliza el dedo sobre la zona, aparecerá :  PAN!
<br>		- si se colocan los dos dedos sobre la zona, aparecerá : PINCH!<br>
<br>

![Screenshot_2017-01-15-22-31-09.png](https://bitbucket.org/repo/jeBbbe/images/2466811074-Screenshot_2017-01-15-22-31-09.png)

<br>
<br>TAG v1.0
<br>	Versión inicial con 2 botones, que al pinchar oscurece o aclara el fondo.